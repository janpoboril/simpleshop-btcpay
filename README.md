# Jak přidat placení Bitcoinem přes BTCPay server do SimpleShop

1. Přidej [platební metodu](https://app.simpleshop.cz/nastaveni/faktury/platebni-metody/) typu _Bankovní převod_ s názvem ve formuláři např. _Bitcoinem_ (popisek ve formuláři musí obsahovat slovo "Bitcoin", číslo účtu zadej stejné jako u reálné platby převodem, protože se pak bude zobrazovat na faktuře).
2. [U konkrétního produktu](https://app.simpleshop.cz/produkty/) na kartě _Formulář_ zapni platební metodu _Bitcoinem_.
3. Na kartě _Ostatní_ do pole _JS, CSS a jiné kódy_ vlož tento kód a uprav data atributy:
```
<script src="https://janpoboril.gitlab.io/simpleshop-btcpay/checkout.js" data-store-id="M2Ppru1LLbVAv3tVtCMXkPxrmZWvtdVfYhm98rMMuTbh" data-server-url="https://btcpay.example.com" data-notify-email="tvuj@email.cz"></script>
```

Zákazník pak bude moci pomocí popupu BTCPay serveru zaplatit. Po zaplacení přijde notifikace na e-mail z `data-notify-email`, ale platbu je potřeba do SimpleShopu vložit ručně ([na stránce objednávky](https://app.simpleshop.cz/faktury/objednavky/) tlačítkem Uhradit).

> Pokud si nevíš rady, tak vždy rád pomůžu: https://www.berubitcoin.cz
