var storeId = document.currentScript.getAttribute('data-store-id');
var serverUrl = document.currentScript.getAttribute('data-server-url');
var notifyEmail = document.currentScript.getAttribute('data-notify-email');

function onBTCPayFormSubmit(event) {
    event.preventDefault();
    event.target.style.opacity = 0.5
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200 && this.responseText) {
            window.btcpay.showInvoice(JSON.parse(this.responseText).invoiceId);
        }
    };
    xhttp.open('POST', event.target.getAttribute('action'), true);
    xhttp.send(new FormData(event.target));
}

document.addEventListener("DOMContentLoaded", function() { 
    const summaryTable = document.getElementsByClassName('summaryAcceptTable')[0]
    const bankPayment = document.getElementsByClassName('bankPayment')[0]
    if (summaryTable && summaryTable.textContent.includes('Bitcoin')) {
        const price = /([\d ]+),\d{2} Kč/.exec(bankPayment.textContent)[1].replace(/\W/g, '')
        const orderId = /Vaše objednávka:\W+(\d+)/.exec(summaryTable.textContent)[1]
        if (!window.btcpay) {
            var script = document.createElement('script');
            script.src = serverUrl + "/modal/btcpay.js";
            document.getElementsByTagName('head')[0].append(script);
        }
        bankPayment.innerHTML = `
            <form method="POST" onsubmit="onBTCPayFormSubmit(event);return false" action="${serverUrl}/api/v1/invoices">
                <input type="hidden" name="storeId" value="${storeId}" />
                <input type="hidden" name="jsonResponse" value="true" />
                <input type="hidden" name="notifyEmail" value="${notifyEmail}" />
                <input type="hidden" name="price" value="${price}" />
                <input type="hidden" name="orderId" value="${orderId}" />
                <input type="hidden" name="currency" value="CZK" />
                <br>
                <input type="submit" class="btn custom-btn positive fill big" name="submit" value="Přejít k platbě (Bitcoin)">
            </form>
        `
    }
})
